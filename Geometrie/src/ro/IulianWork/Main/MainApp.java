package ro.IulianWork.Main;
import ro.IulianWork.FiguriGeometrice.*;
import ro.IulianWork.FiguriGeometrice.FiguriCuPatruLaturi.Dreptunghi;
import ro.IulianWork.FiguriGeometrice.FiguriCuPatruLaturi.Patrat;
import ro.IulianWork.FiguriGeometrice.FiguriCuPatruLaturi.Romb;
import ro.IulianWork.FiguriGeometrice.FiguriCuTreiLaturi.Triunghi;
public class MainApp {

	 public static void main(String[] args) {
	
		 FiguriGeometrice figuri = new FiguriGeometrice();
		 Triunghi tr = new Triunghi();
		 Dreptunghi dr = new Dreptunghi();
		 Patrat pr = new Patrat();
		 Romb rb = new Romb();
		 
		 figuri.AfiseazaPerimetrulFigurilor();
		 System.out.println("Patrat: " + tr.SumaTriunghi(2, 3, 4));
		 System.out.println("Dreptunghi: " + dr.sumaDreptunghi(2, 3));
		 System.out.println("Patrat: " + pr.sumaPatrat(2));
		 System.out.println("Romb: " + rb.sumaRomb(2, 3));
		 
		 System.out.println("----------------------");
		 
		 figuri.AfiseazaAriaFigurilor();
		 System.out.println("Patrat: " +tr.AriaTriunghi(4, 5));
		 System.out.println("Dreptunghi: " +dr.ariaDreptunghi(2, 3));
		 System.out.println("Patrat: " +pr.ariaPatrat(3));
		 System.out.println("Romb: " + rb.ariaRomb(2, 3));
		 
		 System.out.println("----------------------");
	}
}
