package ro.IulianWork.FiguriGeometrice.FiguriCuTreiLaturi;
import ro.IulianWork.FiguriGeometrice.*;
public class Triunghi extends FiguriGeometrice{

	public int SumaTriunghi(int latura1,int latura2,int latura3)
	{
		return latura1+latura2+latura3;
		
	}
	public int AriaTriunghi(int baza,int inaltime)
	{
		return (baza*inaltime)/2;
	}
}
