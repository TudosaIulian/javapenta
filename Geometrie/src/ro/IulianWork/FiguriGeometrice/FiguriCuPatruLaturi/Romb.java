package ro.IulianWork.FiguriGeometrice.FiguriCuPatruLaturi;
import ro.IulianWork.FiguriGeometrice.*;
public class Romb extends FiguriGeometrice{

	public int sumaRomb(int latura1,int latura2)
	{
		return 2*(latura1+latura2);
	}
	public int ariaRomb(int latura1,int inaltime)
	{
		
		return latura1*inaltime;
	}
}
