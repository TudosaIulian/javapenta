package ro.IulianWork.FiguriGeometrice.FiguriCuPatruLaturi;
import ro.IulianWork.FiguriGeometrice.*;

public class Dreptunghi extends FiguriGeometrice{

	public int sumaDreptunghi(int latura,int lungime)
	{
		return (latura+lungime)*2;
		
	}
	public int ariaDreptunghi(int latura,int lungime)
	{
		return latura*lungime;
		
	}
}
